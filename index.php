<html>
<head><A name='Inicio'></A> 
	<title>6 pasos</title>
    <link rel='stylesheet' type='text/css' href='css/toggle.css'>
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel='stylesheet' type='text/css' href='css/font-awesome/css/font-awesome.css'>
	<link rel='stylesheet' type='text/css' href='css/font-awesome/css/font-awesome.min.css'>
    <link rel="shortcut icon" href="img/lo.png">
    <meta name='viewport' content='width=device-width, initial-scale=1'>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="css/edisson.css">
    <link rel="stylesheet" type="text/css" href="css/movil.css">
    <link rel="stylesheet" type="text/css" href="css/tablet.css">
    <link rel="stylesheet" type="text/css" href="css/desktop.css">
    <script type="text/javascript">
        // Scroll to specific values
// scrollTo is the same
window.scroll({
  top: 2500, 
  left: 0, 
  behavior: 'smooth' 
});

// Scroll certain amounts from current position 
window.scrollBy({ 
  top: 100, // could be negative value
  left: 0, 
  behavior: 'smooth' 
});

// Scroll to a certain element
document.querySelector('.hello').scrollIntoView({ 
  behavior: 'smooth' 
});
    </script>

</head>
<body>
    <header id="nav-barE">


    <nav><img src="img/logo1.png" class="lo3 hidden-desktop">
        <a href='#menu' id='toggle'><span></span></a>
        <div id='menu'>
        <ul class='abajo'>

            <a href=""><li><img src="img/logo1.png" class="hidden-phone hidden-tablet"></li></a><br class="hidden-desktop"><br class="hidden-desktop"><br class="hidden-desktop">
            <a href="#Inicio"><li>Inicio</li></a>
            <a href="#Como"><li>Cómo funciona</li></a>
            <a href="#Quiénes"><li>Quiénes somos</li></a>
            <a href="#Contacto"><li>Contacto</li></a>
            <a href="PreguntasFrecuentes"><li>Preguntas frecuentes</li></a>
            <a href="Porquepreferirnos"><li>Por qué preferirnos</li></a>


        </ul>

            <a href="#Contacto"><div id="btnRegisE"  class="hidden-phone hidden-tablet">
                <span  class="hidden-phone hidden-tablet">Registrate</span>
            </div></a>
</div>
    </nav>
</header>
	<div class="home">
		
		<div class="quote-container">
	        <p class="p1">AQUÍ ES DONDE INICIA</p>
	        <p><b class="b1"><span>TU</span> EXPERIENCIA</b></p>
	        <p>PARA ENCONTRAR A TU <b>PAREJA</b></p>
	    </div>
	    <div class="button-container text-center">  
        <br class="hidden-tablet hidden-phone"><br class="hidden-tablet hidden-phone">
       
        <a class="btn btn-lg btn-6o" id="quees" href="#Como">¿CÓMO FUNCIONA?<div class="ripple-wrapper"></div></a><A name='Como'></A>   
        <a class="btn btn-lg btn-6o hidden-xs" href="#Contacto">LLENAR FORMULARIO</a>
        <p class="agencia">
            <span style="font-size: large">Agencia personalizada de Matchmaking</span> <br>
                Personalizado . Confidencial . Exclusivo
                
            </p>
        
 
    </div>
	</div>

<main>
	
            
        
	
	<div class="vid">
        <div class="divcomo1">
            <h1>¿cómo funciona?</h1>
            <p class="como">funciona con tan sólo 6 pasos</p>
            <img src="img/compu.png">
        </div>
        <div class="divcomo2">
            <ol>
                <li>Registro</li>
                <li>Entrevista Personal</li>
                <li>Asesoría en comunicación e imagen</li>
                <li>Búsqueda de tu Match</li>
                <li>La cita con tu Match</li>
                <li>Retroalimentación</li>
            </ol>
        </div>
        
	</div>

	<div id="mensa">
		<A name='Quiénes'></A><span id="mesa1" class="text-center">No se trata de cantidad, sino de calidad</span>
		<span id="mesa2">Nuestras <mark class="matc">Matchmakers</mark> conocen todo de ti y por eso saben exactamente que buscas</span>
	</div>

	<div id="info">
		<div id="info1">
			<span>¿Quiénes sómos?</span>
			<p>En 6 Pasos hacemos que las cosas sucedan no más likes en espera o mensajes sin respuesta. Te presentamos a esa persona para que tengas la oportunidad de conocerla en una cita que nosotros te organizaremos.</p>
            <p>En 6 Pasos conocemos a cada uno de nuestros socios personalmente, y al conocer sus gustos e intereses podemos encontrar personas con mayor afinidad.</p>


            <p>Sabemos que tu éxito profesional te tiene saturado y no tienes tiempo para perderlo en sitios webs o aplicaciones y al conocer sus gustos e intereses podemos encontrar personas con mayor afinidad. Es por eso que presentamos a nuestros socios con candidatos que cuenten con un grado de afinidad mayor al 70%, y es así como formamos parejas compatibles.</p>
			
		</div>
        <div>
            <img src="img/po.jpg" class="pare">
        </div>

	</div>
<p class="image-quote hidden-desktop">
            Nuestros socios son personas exitosas que se han dedicado a su carrera profesional. <br><br>Al no tener tiempo, no pueden invertir en su vida social y de esa manera han dejado atrás su vida social. Es por eso que contratan a los mejores, y no solo en su trabajo sino también para su VIDA PERSONAL. Nuestras Matchmakers son profesionales en encontrar a la persona que tanto llevas esperando.
            
        </p>
<br>
	<div id="info_2">
		<p class="image-quote hidden-phone hidden-tablet">
            Nuestros socios son personas exitosas que se han dedicado a su carrera profesional. <br><br>Al no tener tiempo, no pueden invertir en su vida social y de esa manera han dejado atrás su vida social. Es por eso que contratan a los mejores, y no solo en su trabajo sino también para su VIDA PERSONAL. Nuestras Matchmakers son profesionales en encontrar a la persona que tanto llevas esperando.
            
        </p>
	</div>
	<div class="mensa1">
		<span class="mesa1 text-center">8 de cada 10 Encuentros son satisfactorios</span>
	</div>
    <div class="fondo"> <br><br>
    	<div class="container" style="font-size: 16.6px;">
    		<h1 class="prome"><img src="img/mano.png" style="vertical-align: middle;width: 12%;"> Nuestra promesa</h1>
    		<ul class="ul2">
                <li style="list-style: none;">El equipo de 6 Pasos está dedicado a cada uno de nuestros socios, y este es nuestro compromiso<mark style="font-weight: 900;font-size: 20px;"> :</mark>  </li><br>
    			<li>Guiarte a través de todo tu proceso dentro de 6 Pasos</li>
    			<li>Entender tus objetivos de relación y trabajar contigo para alcanzarlos.</li>
    			<li>Trabajar constantemente a tu lado para identificar y proporcionar los tipos de personas, que podrían ser adecuados para ti. </li>
    			<li>Presentarte sólo aquellos candidatos que tengan un mínimo del 70% de afinidad contigo.</li>
    			<li>Responder rápidamente tus preguntas y necesidades.</li>
    			<li>Comunicarnos contigo de una manera abierta y honesta.</li>
    			<li>Mantener los más altos estándares éticos, personales y profesionales posibles.</li>
    			<li>Somos calificados por cada socio, por lo que tú también calificarás nuestros servicios.</li><A name='Contacto'></A>
    		</ul>
    	</div> 
        <div class="aje">
                
        </div>
        </div>
        <div class="col-md-6">
            <div class="registro">
                <div style="margin: 0 auto;
    width: 95%;
    text-align: left;
    padding: 15px;    overflow: hidden;">
                    <label>Nombre</label>
                    <input type="text" name="nombre" id="nombreAjaxP" required="">
                    <label>Correo</label>
                    <input type="email" name="Correo" id="correoAjaxP" required="">
                    <label>Telefono</label>
                    <input type="number" name="Telefono" id="telefonoAjaxP" required="">
                    <label>Mensaje</label>
                    <input type="text" name="Mensaje" id="mensajeAjaxP" required=""><br>
                    <button name="" value="Enviar" id="boton" class="inline-block input" onclick="ajaxPost()">Registrar</button>
                    <br><br>
                    <div class="gracias1" id="gracias">¡Gracias por registrarte! <i class="fa fa-check-square-o" style="color: #001781;;"></i></div>
                    <div class="gracias1" id="fallo">Debe llenar todos los campos <i class="fa fa-close" style="color: red;"></i></div>
                </div>
            </div>
        </div>
        <div class="col-md-6" style="    margin-top: 20px;">
            <h1 class="contac">Contacto</h1>
            
            <p class="p9">Ecuador</p>
            <p class="p9"><i class="fa fa-envelope"></i> 
                <a href="mailto:info@6pasos.com.ec">info@6pasos.com.ec</a></p>
            <p class="p9"> <i class="fa fa-phone"></i> Teléfono: 0989364645-0999526736</p>
            <p class="p9" class="paso">Estas a pocos pasos de encontrar  <a href="#Contacto" ><mark style="color: #df022d;font-size: 18px;font-weight: bolder;">tu pareja ideal!</mark></a></p>
                <br>
       <div class="text-center">
             <img src="img/mune.png">   
        </div><br><br>
        </div>
        
     </div>  
      
     
</main>
<footer class="footer">
    <br><br>


</footer>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/main.js"></script>
</body>
</html>
